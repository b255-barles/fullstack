const txtFirstName = document.querySelector('#txt-first-name');

const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

const getFullName = () => {
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
}

txtFirstName.addEventListener('keyup', getFullName);
txtLastName.addEventListener('keyup', getFullName);