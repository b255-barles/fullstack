// import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import React from 'react';
import AppNavBar from './components/AppNavBar';
import Error from './components/Error';
// import Highlights from './components/Highlights';
import Home from './pages/Home';
import Courses from './pages/Courses';
import CourseView from './components/CourseView';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import {useState, useEffect} from 'react';

import './App.css';
import {UserProvider} from './UserContext';



function App() {
//  State hook for the use state that defined here for a global scope
//  initialized as an object with properties from the local storage
//  This will be used to store tje user info and will be used fpr validating if a user is logged in on the app or not
  const [user, setUser] = useState({
    // email : localStorage.getItem('email')
    id: null,
    isAdmin: null
  });

// Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  }
  //  used to check if the user information is properly stored upon login
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <Container fluid>          
            <AppNavBar />
            <Routes>     
              <Route path="/" element={<Home/>} />
              <Route path="/home" element={<Home/>} />
              <Route path="/courses" element={<Courses/>} />
              <Route path="/courseView/:courseId" element={<CourseView/>} />

             {/* {(user.email !== null) ? 
              <Route path="/register" element={<Courses/>} />
              :*/}
              <Route path="/register" element={<Register/>} />


              }
              <Route path="/login" element={<Login/>} />
              <Route path="/logout" element={<Logout/>} />
              <Route path="*" element={<Error/>} />

              
            </Routes> 
          
        </Container>
      </Router>
    </UserProvider>
  );
}



export default App;
