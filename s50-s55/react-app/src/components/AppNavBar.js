import Container from 'react-bootstrap/Container';
import { useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import React from 'react';
import UserContext from '../UserContext';



export default function AppNavBar(){
	// State to store the user infromation stored in the login page
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);
	const { user } = useContext(UserContext);

	return(
		<Navbar bg="success" expand="lg">
		    <Container fluid>
		        <Navbar.Brand as={Link} to="/">Zuitt Booking</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		        <Navbar.Collapse id="basic-navbar-nav">
		            <Nav className="mr-auto">
		            <Nav.Link as={NavLink} to="/home">Home</Nav.Link>
		            <Nav.Link as={NavLink} to="/courses" exact="true">Courses</Nav.Link>
		            {(user.id !== null) ? 
		            
		            <Nav.Link as={NavLink} to="/logout" exact="true">Logout </Nav.Link>

		            

		            :
		            <React.Fragment>
		            	<Nav.Link as={NavLink} to="/login" exact="true"> Login </Nav.Link>
		            	<Nav.Link as={NavLink} to="/register" exact="true"> Register </Nav.Link>
		            </React.Fragment>
		        	}
		            
		            </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>
	)
}

