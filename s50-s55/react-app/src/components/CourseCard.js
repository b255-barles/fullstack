import {Button, Card } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}){
    // Checks to see if the data was successfully passed
	// console.log(course);
    // Every component receives information in a form of an object
    // console.log(typeof course);   

    //  use the state hook for this component to be able to store its state
    // States are used to keep track infromation related to individual components
    const [count, setCount]   = useState(0);
    const [seats, seatCount] = useState(30);

    console.log("seatCount", seats);
    

    //  using the state hook returns an array with the first element being a value and the second element as function that is used to change the value of the first element

    // console.log(useState(0));
    console.log("counter" ,count);
    // Function that keeps track of the enrollees for a course
    // by default JS is synchronous it executes code from the top of the file all the way down 
    // The setter function for useStates are asynchronous allowing it to execute separatly from other codes in the program
    // The setCOunt function is being executed while the console.log is already completed
    /*function enroll(){
        setCount(count + 1);
       
        seatCount(seats - 1);*/

/*
        if (seats === 0){
            alert("No more Seats");
            setCount(30);
            seatCount(0);
        }*/
       
       /* console.log('Enrollees' + count)
        console.log('SEATS' + seats)*/
        // console.log(setSeats)
    

    // Define a "useEffect" hook to have the CourseCard component perform a certain task after every DOM update
    //  This is run automatically after initial render and for every dom UPDATE
    // checjubg for the availability for enrollment of a course is better suited here
    // React will re-run this effect ONLY if aby of the valyes contained in this array has changed from the last render/update
    /*useEffect(() => {
        if(seats === 0){
            
            // setIsOpen(false);
        }
    }, [seats])*/



    const {name, description, price, _id} = courseProp;

    return(
		<Card className="cardHighlight p-3">
                <Card.Body>
                	<Card.Title>
                            <h2>{name}</h2>
                        </Card.Title>
                        
                    <Card.Text>
                    <p>Description: <br/> 
                    </p>
                    {description}

                    <p>Price: <br />
                    PHP</p> {price}
                    </Card.Text>
                    <Card.Text>Enrollees: {count}</Card.Text>
                    <Card.Text>Seats: {seats}</Card.Text>

                    <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
                </Card.Body>
        </Card>
       )
    
}

CourseCard.propTypes = {
    //  The shape method is used to heck if a prop object conforms to a specific shape
    course: PropTypes.shape({
        // define the properties 
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,

    })
}