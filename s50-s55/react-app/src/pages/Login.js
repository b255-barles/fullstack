import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(props){
	//  allows us to consume the user context object and it's properties to use for user validation
	const { user, setUser } = useContext(UserContext);


	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// State to determine whether submit vuttion is enabled or not
	const [isActive, setIsActive] = useState('');

	console.log(email);
	console.log(password)

	function loginUser(e){
			//  Prevents page redirection via form submission
			e.preventDefault();

			//  process a fetch request to the corresponding backend API
			//  the header information content-type is used to specify that the information being sent to the backend will be sent in the form of JSON
			//  The fetch request  will communicate with our backend application providing it with a stringified JSON
			fetch('http://localhost:4000/users/login', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify ({
					email: email,
					password: password
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				// if no user info is ound, the access property will not be available and will return undeined
				if(typeof data.access !== "undefined"){
					localStorage.setItem('token', data.access);
					retrieveUserDetails(data.access);

					Swal.fire({
						title: "Login Successful",
						icon: "success",
						text: "Welcome to Zuitt!"
					});
				} else {
					Swal.fire({
						title: "Authentication Failed",
						icon: "error",
						text: "Check your details and try again."
					});
				};
			})

			// set the email of the authenticated user in teh local storage

			// localStorage.setItem('email', email);

			// Set the global user state to have properties obtained from local storage
			// Through access to the user info can be done via the localStorage this is necessary to update the user state which will help update the app component and rerender it to avoid refreshing the page
			// When state change components are rerendered abd th AppNavBar component will be upadted based on the user credentials
			/*setUser({
				email: localStorage.getItem('email')
			});*/

			// clear input fields
			setEmail('');
			setPassword('');
			

			alert(`Hello, ${email}! You are now logged in!`)
		}
	// 

	const retrieveUserDetails = (token) => {
		// The token will be sent as part of the requests header information
		fetch ('http://localhost:4000/users/details', {
			headers: {
				Authorization: `Bearer ${ token }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}	


	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if((email !== '' && password !== '')){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [email, password])
	

		return(
			// console.log("eyy" user.id)
				(user.id !== null) ?
				<Navigate to="/courses"/>
				:
				<Form onSubmit={(e) => loginUser(e)}>
			        <Form.Group controlId="userEmail">
			            <Form.Label>Email address</Form.Label>
			            <Form.Control 
			                type="email" 
			                placeholder="Enter email" 
			                value={email}
			                onChange={e => setEmail(e.target.value)}
			                required
			            />
			           
			        </Form.Group>

			        <Form.Group controlId="password">
			            <Form.Label>Password</Form.Label>
			            <Form.Control 
			                type="password" 
			                placeholder="Password" 
			                value={password}
			                onChange={e => setPassword(e.target.value)}
			                required
			            />
			        </Form.Group>

			        <br />
			        {isActive ?
			        	 <Button variant="success" type="submit" id="submitBtn">
			                Submit
			            </Button>
			          :
			            <Button variant="success" type="submit" id="submitBtn" disabled>
			                Submit
			            </Button>
			    	}
			           
			        	

			    	
			            
			    </Form>


		)

	
}


