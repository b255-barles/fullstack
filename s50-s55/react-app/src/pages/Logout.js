import {useContext, useEffect } from 'react';

import { Navigate} from 'react-router-dom';
import UserContext from '../UserContext';


export default function Logout(){
	//  Consume the UserContext object and destructure it to access the user state and unsetUser function from the context provider
	const{unsetUser, setUser} = useContext(UserContext)

	// clear the localStorage og the users info
	unsetUser()

	// placing the "SetUser " setter function inside of a useEffect is necessary because of updates within React JS that a state of another component cannot be updated while tring to render a diff components
	// By adding the useEffect, this will allow the logout page to render first before triggering the useEffect which changes the state of the user


	useEffect(() => {
		// Set the user state back to its original vaue
		setUser({id: null});
	})

	return(
		<Navigate to='/login' />
	)
}