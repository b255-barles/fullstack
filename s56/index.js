function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.


      // Check if the letter is a single character
      if (letter.length === 1) {
        // Convert the sentence into an array of characters
        const chars = sentence.split('');

        // Loop through each character and check if it matches the letter
        for (let i = 0; i < chars.length; i++) {
          if (chars[i] === letter) {
            result++;
          }
        }

        // Return the count
        return result;
      } else {
        // Return undefined if the letter is not a single character
        return undefined;
      }
    }




    



function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    
      // Convert the text to lowercase to disregard casing
      text = text.toLowerCase();

      // Loop through each character in the text
      for (let i = 0; i < text.length; i++) {
        // Check if the current character appears more than once in the text
        if (text.indexOf(text[i]) !== i) {
          return false;
        }
      }

      // If we reach this point, there are no repeating letters in the text
      return true;
    
};

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    price
    if(age >= 13 && age < 21){
        let discount = Math.round(price * 0.8);
        return discount.toString();

    }else if(age >= 22 && age <= 64){
        let totalPrice = Math.round(price);
        return totalPrice.toString();
    } else {
        console.log("error age is either young or old")
    }

    
}

function findHotCategories(items) {
  const result = [];
  const addedCategories = new Set(); // to keep track of categories already added
  
  for (let i = 0; i < items.length; i++) {
    const item = items[i];
    
    // check if item has no stocks
    if (item.stocks === 0) {
      // check if category has not been added yet
      if (!addedCategories.has(item.category)) {
        result.push(item.category);
        addedCategories.add(item.category);
      }
    }
  }

  return result;
}
  const items = [
  { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
  { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
  { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
  { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
  { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
];

console.log(findHotCategories(items)); // expected output: ['toiletries', 'gadgets']

function findFlyingVoters(candidateA, candidateB) {
  // Find voters who voted for both candidate A and candidate B.
  const flyingVoters = [];

  for (let i = 0; i < candidateA.length; i++) {
    const voter = candidateA[i];

    for (let j = 0; j < candidateB.length; j++) {
      if (voter === candidateB[j]) {
        flyingVoters.push(voter);
        break;
      }
    }
  }

  return flyingVoters;
}

const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

const result = findFlyingVoters(candidateA, candidateB);
console.log(result); // ['LIWf1l', 'V2hjZH']



/*
module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};
*/
let myObj = {
    name: 'j',
    age: 25,
    job: 'dv',
    hobbies: ["reading", "playing"],
    sH: function(){
        console.log("asdasd", this.name)
    }
};
console.log(myObj.hobbies[2]);