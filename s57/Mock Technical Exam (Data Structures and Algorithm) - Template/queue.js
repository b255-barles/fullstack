let collection = [];

// Write the queue functions below.

function print(){
    return collection;

}

console.log(print());

// for enqueue

function enqueue(item){
    collection.push(item)
    return collection;
}

enqueue();
console.log( collection);

// dequeue

function dequeue(item){
    
    collection.shift();
    return collection;
}


// front
function front() {
  return collection[0];
}


// size

function size() {
    return collection.length;
}

console.log(size());

// isEmpty

function isEmpty(){
    if (collection.length === 0){
        return true;
    }else{
        collection = [];
        return false;
    }
}

console.log(isEmpty());
console.log(collection)


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};